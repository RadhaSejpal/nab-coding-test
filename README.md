Nab Coding Challenge

Problem Statement :
Create a mock library to simulate 50 stock names and their prices. Simulate the price movement in either direction every 5 seconds.
Develop an ASP.Net page to display the price list. The web page should allow the user to:
a) Filter the price list by name. For e.g. typing in XYZ should fetch all names containing XYZ.
b) Refresh the prices
Approach :
1.	Created Web Application that uses ASP.NET SignalR to provide server broadcast functionality. 
2.	Created a hub class to push content to clients. 
3.	Used the SignalR jQuery library in a web page to send messages and display updates from the hub.

Background :
Server broadcast messages that are sent to clients are initiated by the server, hence the updates of Stock Prices are signaled by Server to Client. 
SignalR uses the WebSocket transport where available and falls back to older transports where necessary. Transport layers used by SignalR are as below
1. WebSocket 
2. Server Sent Events, also known as EventSource 
3. Forever Frame (for Internet Explorer only). 
4. Ajax long polling.

Prerequities :
1.	Enable WebSocket by following below steps- 
a.	Navigate to: Control Panel -> Program & Features -> Turn Windows Features On & Off -> Internet Information Services -> World Wide Web services -> Application Development Features 
b.	Select Websocket Protocol + OK
2.	Set StockSimulator.html under views folder as the StartUp page
3.	Search functionality will trigger only on the 3rd character entered in the box.

Software versions used :
Visual Studio 2015 .
NET Framework 4.5.2 
SignalR version 2 
JQuery 1.10.2 
JQuery SignalR 2.3.0 
JavaScript 
Ninject ( Dependency Injection) CsvHelper

Features Used: 
Dependency Injection 
Singleton Design Pattern 
CsvParser 
Unit Test Cases using xUnit & Moq.

Improvements :
Add NLog feature for logging
Higlighing the Stock that is updating.

