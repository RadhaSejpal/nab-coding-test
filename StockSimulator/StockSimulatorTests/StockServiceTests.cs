﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;
using StockSimulator.Csv;
using StockSimulator.Hubs;
using StockSimulator.Stocks;
using Xunit;

namespace StockSimulatorTests
{
    public class StockServiceTests
    {
        [Fact]
        public void StockServiceCtorTests_ReturnsNullStocks()
        {
            //Get Mock Clients
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();

            //Get Mock CsvParser
            var mockParser = new Mock<ICsvParser<Stock>>();

            //Return Mock Stocks on Parser request
            mockParser.Setup(x => x.GetCsvContent(It.IsAny<string>()))
                .Returns<IEnumerable<Stock>>(null); // return null stocks

            //initialize stock service object
            var stockService = new StockService(mockClients.Object,mockParser.Object);

            //Call stock service GetAllStocks function
            var stocks = stockService.GetAllStocks();

            //stocks shld be null
            Assert.Null(stocks);

        }
        [Fact]
        public void StockServiceCtorTests_ReturnsNotNullStocks()
        {
            //Get Mock Clients
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();

            //Get Mock CsvParser
            var mockParser = new Mock<ICsvParser<Stock>>();
            
            //stock list
            var moqStocks = new List<Stock>() {new Stock() {StockName = "APPLE", StockSymbol = "APPL", StockPrice = 50.00m} };

            //Return Mock Stocks on Parser request
            mockParser.Setup(x => x.GetCsvContent(It.IsAny<string>())).Returns(moqStocks);
                
            //initialize stock service object
            var stockService = new StockService(mockClients.Object, mockParser.Object);

            //Call stock service GetAllStocks function
            var stocks = stockService.GetAllStocks();

            //stocks shld be null
            Assert.NotNull(stocks);

         }
        [Fact]
        public void StockServiceCtorTests_ReturnsAppleStocks()
        {
            //Get Mock Clients
            var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();

            //Get Mock CsvParser
            var mockParser = new Mock<ICsvParser<Stock>>();

            //stock list
            var moqStocks = new List<Stock>() { new Stock() { StockName = "APPLE", StockSymbol = "APPL", StockPrice = 50.00m } };

            //Return Mock Stocks on Parser request
            mockParser.Setup(x => x.GetCsvContent(It.IsAny<string>())).Returns(moqStocks);

            //initialize stock service object
            var stockService = new StockService(mockClients.Object, mockParser.Object);

            //Call stock service GetAllStocks function
            var stocks = stockService.GetAllStocks();

            //stocks shld be null
            Assert.Equal("APPL",stocks.ToList()[0].StockSymbol);

        }
    }
}
