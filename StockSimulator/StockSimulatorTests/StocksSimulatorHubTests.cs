﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Hubs;
using Moq;
using StockSimulator.Hubs;
using StockSimulator.Stocks;
using Xunit;

namespace StockSimulatorTests
{
    public class StocksSimulatorHubTests
    {
        [Fact]
        public void SimulatorHubTest_ReturnsNotNullStocks()
        {
                //Mock Stock Service
                var mockService = new Mock<IStockService>();
                //Mock SimulatorHub
                var hub = new Mock<StockSimulatorHub>(mockService.Object);
                //Get Mock Clients
                var mockClients = new Mock<IHubCallerConnectionContext<dynamic>>();
                // Set as Hub's mock Clients
                hub.Object.Clients = mockClients.Object;
                //Call Hub GetAllStocks Function
                var stocks = hub.Object.GetAllStocks();
                Assert.NotNull(stocks);
        }
    }
}
