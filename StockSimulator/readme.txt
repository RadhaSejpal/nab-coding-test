Problem Statement : 

Create a mock library to simulate 50 stock names and their prices. Simulate the price movement in either direction every 5 seconds.

Develop an ASP.Net page to display the price list. The web page should allow the user to:

a)      Filter the price list by name. For e.g. typing in XYZ should fetch all names containing XYZ.

b)      Refresh the prices

Approach :
Created Web Application that uses ASP.NET SignalR 2 to provide server broadcast functionality. 
1. Adding the SignalR library to an ASP.NET web application.
2. Creating a hub class to push content to clients.
3. Creating an OWIN startup class to configure the application.
4. Using the SignalR jQuery library in a web page to send messages and display updates from the hub.

Background:
Server broadcast messages that are sent to clients are initiated by the server, hence the updates of Stock Prices are signaled by Server to Client.
SignalR uses the new WebSocket transport where available and falls back to older transports where necessary
1. WebSocket
2. Server Sent Events, also known as EventSource
3. Forever Frame (for Internet Explorer only).
4. Ajax long polling.

Prerequities :
Enable WebSocket by following below steps-
1. Click the Windows button
2. Type: Turn windows features on or off
3. Navigate to: Internet Information Services –> world Wide Web services –> Application Development Features
4. Select “Websocket Protocol” + OK


Software versions used :
Visual Studio 2015
.NET Framework 4.5.2
SignalR version 2
JQuery 1.10.2
JQuery SignalR 2.3.0
JavaScript
Ninject ( Dependency Injection)
CsvHelper

Features Used:
SignalR
Dependency Injection
Singleton Design Pattern
CsvParser
Unit Test Cases using xUnit & Moq.

Improvements:
Add NLog feature for logging.
Exception Handling
Higlighing the Stock that is updating.




