﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using StockSimulator.Stocks;

namespace StockSimulator.Hubs
{
    [HubName("StockSimulatorHub")]
    public class StockSimulatorHub : Hub, ISimulatorHub
    {
        private readonly IStockService _stockTicker;
        
        public StockSimulatorHub(IStockService stockTicker)
        {
            if (stockTicker == null)
            {
                throw new ArgumentNullException("StockService not found");
            }
            _stockTicker = stockTicker;
        }

        public IEnumerable<Stock> GetAllStocks()
        {
            return _stockTicker.GetAllStocks();
        }
    }
}