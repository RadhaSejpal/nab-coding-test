﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockSimulator.Stocks;

namespace StockSimulator.Hubs
{
    public interface ISimulatorHub
    {
        IEnumerable<Stock> GetAllStocks();
    }
}
