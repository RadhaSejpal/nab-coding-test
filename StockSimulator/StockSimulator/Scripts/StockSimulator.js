﻿// A simple templating method for replacing placeholders enclosed in curly braces.
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {

    var ticker = $.connection.StockSimulatorHub, // the generated client-side hub proxy
        $stockTable = $('#stockTable'),
        $stockTableBody = $stockTable.find('tbody'),
        rowTemplate = '<tr data-symbol="{StockSymbol}"><td>{StockSymbol}</td><td>{StockPrice}</td><td>{StockName}</td></tr>';

    function formatStock(stock) {
        return $.extend(stock, {
            StockPrice: stock.StockPrice.toFixed(2)
        });
    }

    function init() {
        ticker.server.getAllStocks().done(function (stocks) {
            $stockTableBody.empty();
            $.each(stocks, function () {
                var stock = formatStock(this);
                $stockTableBody.append(rowTemplate.supplant(stock));
            });
        });
    }

    // Add a client-side hub method that the server will call
    ticker.client.updateStockPrice = function (stock) {
        var displayStock = formatStock(stock),
        $row = $(rowTemplate.supplant(displayStock));
        $stockTableBody.find('tr[data-symbol=' + stock.StockSymbol + ']')
            .replaceWith($row);
    }
    
    //Search in the table
    $("#search").bind('keyup', function () {
        rowHighlight();
    });

    $("#search").focusout(function () {
        if ($("#search").val() == 0) {
            $("#searchIcon").show();
        }
        rowHighlight();
    });

    function rowHighlight() {
        var searchTerm = $("#search input").val().toLowerCase();
        var tableRows = $("#stockLists").children();

        for (var i = 0; i < tableRows.length; i++) {
            var particularRow = tableRows[i];
            if (searchTerm.length > 2) {
                $("#searchIcon").hide();
                if (tableRows[i].innerText.toLowerCase().indexOf(searchTerm) != -1) {
                    particularRow.style.backgroundColor = "#90ee90";
                }
                else {
                    particularRow.style.backgroundColor = "transparent";
                }
            }
            else {
                $("#searchIcon").show();
                particularRow.style.backgroundColor = "transparent";

            }
        }
    }

   // Start the connection
    $.connection.hub.start().done(init);

});