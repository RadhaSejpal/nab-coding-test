﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Owin;
using Ninject;
using Owin;
using StockSimulator.Csv;
using StockSimulator.Hubs;
using StockSimulator.Stocks;

[assembly: OwinStartup(typeof(StockSimulator.Startup))]

namespace StockSimulator
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
           //Ninject Resolver
            var kernel = new StandardKernel();
            var resolver = new NinjectSignalRDependencyResolver(kernel);
            kernel.Bind<IStockService>().To<StockService>() // Bind to StockService.
                .InSingletonScope();  // Make it a singleton object.
            kernel.Bind<ISimulatorHub>().To<StockSimulatorHub>();
            kernel.Bind(typeof(ICsvParser<>)).To(typeof(CsvParser))
                .InSingletonScope(); // Make it a singleton object.
            kernel.Bind(typeof(IHubConnectionContext<dynamic>)).ToMethod(context =>
                resolver.Resolve<IConnectionManager>().GetHubContext<StockSimulatorHub>().Clients
            ).WhenInjectedInto<IStockService>();

            var config = new HubConfiguration {Resolver = resolver};
            ConfigureSignalR(app, config);
        }
        public static void ConfigureSignalR(IAppBuilder app, HubConfiguration config)
        {
            app.MapSignalR(config);
        }
    }
}
