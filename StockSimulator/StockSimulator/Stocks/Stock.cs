﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockSimulator.Stocks
{
    public class Stock
    {
        public string StockName { get; set; }
        public string StockSymbol { get; set; }
        public decimal StockPrice { get; set; }
        
    }
}