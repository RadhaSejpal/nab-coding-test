﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using StockSimulator.Csv;
using StockSimulator.Hubs;
using StockSimulator.Stocks;

namespace StockSimulator.Stocks
{
    /// <summary>
    /// Stock Service responsible to generate and update the Stock Prices every 5 seconds.
    /// Singleton Instance.
    /// </summary>
    public class StockService : IStockService
    {
        
        //ThreadSafe dictionary to hold the Stocks
        private readonly ConcurrentDictionary<string, Stock> _stocks = new ConcurrentDictionary<string, Stock>();

        //lock object used while generating the stock price
        private readonly object _stockLock = new object();

        //Only update the stock price by 20% in either direction.
        private readonly double _range = .002;

        //TimeSpan to update the stock price every 5 seconds.
        private readonly TimeSpan _interval = TimeSpan.FromMilliseconds(500);

        //Randomly pick the stock that needs to be updated.
        private readonly Random _stockpicker = new Random();

        //Timer to update the stock price 
        private readonly Timer _stockTimer;

        //Flag to check before updating the stock price that no other thread has acquaried it.
        private volatile bool _updatingStockPrices = false;

        //CsvParser to read sample 100 stocks from Csv File.
        private readonly ICsvParser<Stock> _csvParser;

        //Hub client list to publish the update .
        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }
        public StockService(IHubConnectionContext<dynamic> clients,ICsvParser<Stock> csvParser)
        {
            //CHeck if client object is null, throw exception.
            if (clients == null)
            {
                throw new ArgumentNullException("Clients Not Found");
            }
            Clients = clients;

            //Check if CsvParser is null, throw exception
            if (csvParser == null)
            {
                throw new ArgumentNullException("Csv Parser Not Found");
            }

            _csvParser = csvParser;

            //Clear stocks dictionary
            _stocks.Clear();

            //Retrieve list of Stocks from Csv Parser
            var stocks = _csvParser.GetCsvContent("/stocksList.csv");
            
            //Check is Stock object is not null , try to add in the stock dictionary in threadsafe manner.
            stocks?.ForEach(stock => _stocks.TryAdd(stock.StockSymbol, stock));

            //Start the stock timer to generate the stock price every 5 seconds.
            _stockTimer = new Timer(GenerateStockPrice, null, _interval, _interval);

        }
        public IEnumerable<Stock> GetAllStocks()
        {
            return _stocks.Values;
        }

        /// <summary>
        /// Publish Stock update to Clients
        /// </summary>
        /// <param name="stock"></param>
        private void PublishStockPrice(Stock stock)
        {
            Clients.All.updateStockPrice(stock);
        }
        /// <summary>
        /// Update Stock Prices every 5 seconds
        /// </summary>
        /// <param name="state">to be used to set the callback method but currently set to null</param>
        private void GenerateStockPrice(object state)
        {
            //Acquire the lock
            lock (_stockLock)
            {
                //check if other thread is updating the stock price.
                if (!_updatingStockPrices)
                {
                    _updatingStockPrices = true;

                    foreach (var stock in _stocks.Values)
                    {
                        if (TryUpdateStockPrice(stock))
                        {
                            //Publish the Stock updated price to Clients
                            PublishStockPrice(stock);
                        }
                    }

                    _updatingStockPrices = false;
                }
            }
        }
        private bool TryUpdateStockPrice(Stock stock)
        {
            // Randomly choose whether to update this stock or not
            var r = _stockpicker.NextDouble();
            if (r > .1)
            {
                return false;
            }

            // Update the stock price by a random factor of the range percent
            var random = new Random((int)Math.Floor(stock.StockPrice));
            var percentChange = random.NextDouble() * _range;
            var pos = random.NextDouble() > .51;
            var change = Math.Round(stock.StockPrice * (decimal)percentChange, 2);
            change = pos ? change : -change;

            //Update the stock price by change value.
            stock.StockPrice += change;
            return true;
        }

    }
}