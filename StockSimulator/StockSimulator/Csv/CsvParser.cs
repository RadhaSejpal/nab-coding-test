﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using CsvHelper;
using StockSimulator.Stocks;

namespace StockSimulator.Csv
{
    public interface ICsvParser<T>
    {
        List<T> GetCsvContent(string filePath);
    }
    public class CsvParser:ICsvParser<Stock>
    {
        private readonly List<Stock> _stocks = new List<Stock>();
        
        public List<Stock> GetCsvContent(string filePath)
        {
            try
            {
                if (_stocks.Count == 0 && !string.IsNullOrEmpty(filePath))
                {
                    using (TextReader reader = File.OpenText(GetFilePath(filePath)))
                    {
                        CsvReader csv = new CsvReader(reader);
                        csv.Configuration.Delimiter = ",";
                        csv.Configuration.MissingFieldFound = null;
                        while (csv.Read())
                        {
                            Stock record = csv.GetRecord<Stock>();
                            _stocks.Add(record);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log exception;
            }

            return _stocks;
        }
        private string GetFilePath(string filePath)
        {
            return HttpContext.Current.Server.MapPath(filePath);
        }
    }
    //Todo
    //Test Cases
    //Search Functionality
    //Highlighter for the stock updating
}